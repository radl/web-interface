# RADL Web Interface

Implementation of the **Web Interface** of the **RADL System**, using the following technologies:
- [NodeJS](https://nodejs.org/en/) Environment
- [Webpack](https://webpack.js.org/) Module Bundler
- [GraphQL](https://graphql.org/) Query Language
- [TypeScript](https://www.typescriptlang.org/) Language
- [Express](https://expressjs.com/) Framework
- [Sass](https://sass-lang.com/) (CSS extension)
- [React](https://reactjs.org/) Framework

Additional packages:
- [Apollo Client](https://www.apollographql.com/docs/react/) Library
- [React Router](https://reactrouter.com/) Library
- [Recharts](https://recharts.org/en-US/) Library

The **Web Interface** displays the information from the **Sensor Nodes**:
- `/` : List of all Sensor Nodes and their status
- `/node/:NODE_ID` : Displays a Sensor Node status and a chart of logs for each of its sensors

A video demonstration is available [here](https://1drv.ms/v/s!AgT90Ys_KiI_gcdJ_g59QTnwO_WUWA?e=mBfcBX).

## Getting Started

#### Requirements

This project was implemented with the following:
- [NodeJS](https://nodejs.org/en/) (v14.5.0)
- [Yarn](https://yarnpkg.com/) (v1.22.4)

It is advised to have the same setup to run this project.

#### Usage

To setup the project:
```shell script
yarn
```
To run the project in development:
```shell script
yarn run dev
```
To build and run the project:
```shell script
yarn run build
yarn start
```
To clean the project:
```shell script
yarn run clean
```
