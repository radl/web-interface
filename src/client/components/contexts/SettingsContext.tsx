import React, { useState } from 'react'

export type SettingsID = 'temperature' | 'humidity' | 'co2Level' | 'lightIntensity' | 'soundLevel' | 'motionDetector'

export type Settings = {
    temperature: boolean
    humidity: boolean
    co2Level: boolean
    lightIntensity: boolean
    soundLevel: boolean
    motionDetector: boolean
}

type Context = {
    state: Settings,
    setState: (dateRange: Settings) => void
}

const initialSettings = {
    temperature: false,
    humidity: false,
    co2Level: false,
    lightIntensity: false,
    soundLevel: false,
    motionDetector: false
}

export const Context = React.createContext<Context>({
    state: initialSettings,
    setState: () => { }
})

export const Provider: React.FC = ({ children }) => {
    const [state, setState] = useState<Settings>(initialSettings)

    return (
        <Context.Provider value={{ state, setState }}>
            { children }
        </Context.Provider>
    )
}
