import React, { useState } from 'react'

export type DateRange = {
    from: Date
    to: Date
}

type Context = {
    state: DateRange,
    setState: (dateRange: DateRange) => void
}

const substractDate = (date1: Date, date2: Date) => new Date(Math.abs(date1.valueOf() - date2.valueOf()))

const initialDateRange = {
    from: substractDate(new Date(), new Date(15000)),
    to: new Date()
}

export const Context = React.createContext<Context>({
    state: initialDateRange,
    setState: () => { }
})

export const Provider: React.FC = ({ children }) => {
    const [state, setState] = useState<DateRange>(initialDateRange)

    return (
        <Context.Provider value={{ state, setState }}>
            { children }
        </Context.Provider>
    )
}

export const withContext = <P extends object>(Component: React.FC<P>): React.FC<P> => (props) => (
    <Provider>
        <Component {...props as P}/>
    </Provider>
)
