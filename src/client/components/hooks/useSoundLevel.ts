import { SoundLevel } from '../../graphql/__types__/SoundLevel'
import { QUERY_SOUND_LEVEL } from '../../graphql/queries'
import useSensor from './useSensor'

const useSoundLevel = (serialNumber: string) => useSensor<SoundLevel>(
    serialNumber,
    QUERY_SOUND_LEVEL,
    (data?: SoundLevel) => (data ? (data.node ? data.node.sensors.soundLevel : []) : [])
)

export default useSoundLevel
