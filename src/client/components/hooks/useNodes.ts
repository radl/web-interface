import { useQuery } from '@apollo/react-hooks'
import { useHistory } from 'react-router-dom'

import { QUERY_NODES } from '../../graphql/queries'
import { Nodes } from '../../graphql/__types__/Nodes'

const useNodes = () => {
    const { push: goTo } = useHistory()
    const { data, loading } = useQuery<Nodes>(
        QUERY_NODES,
        {
            fetchPolicy: 'cache-and-network',
            onError: (err) => {
                if (err.networkError) goTo('/error', { error: "System API not reachable" })
                else if (err.graphQLErrors.length > 0) goTo('/error', { error: "Could not fetch the sensor nodes"})
                else goTo('error')
            }
        }
    )

    return { data: (data ? data.nodes : []), loading }
}

export default useNodes
