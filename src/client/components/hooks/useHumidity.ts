import { Humidity } from '../../graphql/__types__/Humidity'
import { QUERY_HUMIDITY } from '../../graphql/queries'
import useSensor from './useSensor'

const useHumidity = (serialNumber: string) => useSensor<Humidity>(
    serialNumber,
    QUERY_HUMIDITY,
    (data?: Humidity) => (data ? (data.node ? data.node.sensors.humidity : []) : [])
)

export default useHumidity
