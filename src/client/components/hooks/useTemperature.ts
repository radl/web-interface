import { Temperature } from '../../graphql/__types__/Temperature'
import { QUERY_TEMPERATURE } from '../../graphql/queries'
import useSensor from './useSensor'

const useTemperature = (serialNumber: string) => useSensor<Temperature>(
    serialNumber,
    QUERY_TEMPERATURE,
    (data?: Temperature) => (data ? (data.node ? data.node.sensors.temperature : []) : [])
)

export default useTemperature
