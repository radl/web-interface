import { CO2Level } from '../../graphql/__types__/CO2Level'
import { QUERY_CO2_LEVEL } from '../../graphql/queries'
import useSensor from './useSensor'

const useCO2Level = (serialNumber: string) => useSensor<CO2Level>(
    serialNumber,
    QUERY_CO2_LEVEL,
    (data?: CO2Level) => (data ? (data.node ? data.node.sensors.co2Level : []) : [])
)

export default useCO2Level
