import { LightIntensity } from '../../graphql/__types__/LightIntensity'
import { QUERY_LIGHT_INTENSITY } from '../../graphql/queries'
import useSensor from './useSensor'

const useLightIntensity = (serialNumber: string) => useSensor<LightIntensity>(
    serialNumber,
    QUERY_LIGHT_INTENSITY,
    (data?: LightIntensity) => (data ? (data.node ? data.node.sensors.lightIntensity : []) : [])
)

export default useLightIntensity
