import { useContext } from 'react'
import { useLazyQuery } from '@apollo/react-hooks'
import { DocumentNode } from 'apollo-boost'
import { useHistory } from 'react-router-dom'

import { Context } from '../contexts/DateRangeContext'
import { Temperature_node_sensors_temperature as Temperature, TemperatureVariables } from '../../graphql/__types__/Temperature'
import { Humidity_node_sensors_humidity as Humidity } from '../../graphql/__types__/Humidity'
import { CO2Level_node_sensors_co2Level as CO2Level } from '../../graphql/__types__/CO2Level'
import { LightIntensity_node_sensors_lightIntensity as LightIntensity } from '../../graphql/__types__/LightIntensity'
import { SoundLevel_node_sensors_soundLevel as SoundLevel } from '../../graphql/__types__/SoundLevel'
import { MotionDetector_node_sensors_motionDetector as MotionDetector } from '../../graphql/__types__/MotionDetector'

type TVariables = TemperatureVariables

export type SensorLog = Temperature | Humidity | CO2Level | LightIntensity | SoundLevel | MotionDetector

export type SensorLogs = SensorLog[]

export type ChartValue = {
    time: number
    value: number
}

export type ChartValues = ChartValue[]

export type Result = [
    () => void,
    {
        data: ChartValues,
        loading: boolean
    }
]

const formatChartData = (data: SensorLogs): ChartValues => data.map(elt => ({
    time: new Date(elt.time).getTime(),
    value: (typeof elt.value === 'number') ? elt.value : (elt.value ? 1 : 0)
})).sort( (a, b) => a.time - b.time )

const useSensor = <TData>(serialNumber: string, QUERY: DocumentNode, formatData: (data?: TData) => SensorLogs): Result => {
    const { push: goTo } = useHistory()
    const { state } = useContext(Context)
    const [fetch, { data, loading }] = useLazyQuery<TData, TVariables>(
        QUERY,
        {
            variables: {
                serialNumber,
                from: state.from.toISOString(),
                to: state.to.toISOString()
            },
            fetchPolicy: 'cache-and-network',
            onError: (err) => {
                if (err.networkError) goTo('/error', { error: "System API not reachable" })
                else if (err.graphQLErrors.length > 0) goTo('/error', { error: `Could not fetch the logs`})
                else goTo('error')
            }
        }
    )

    return [fetch, { data: formatChartData(formatData(data)), loading }]
}

export default useSensor
