import { MotionDetector } from '../../graphql/__types__/MotionDetector'
import { QUERY_MOTION_DETECTOR } from '../../graphql/queries'
import useSensor from './useSensor'

const useMotionDetector = (serialNumber: string) => useSensor<MotionDetector>(
    serialNumber,
    QUERY_MOTION_DETECTOR,
    (data?: MotionDetector) => (data ? (data.node ? data.node.sensors.motionDetector : []) : [])
)

export default useMotionDetector
