import { useQuery } from '@apollo/react-hooks'
import { useHistory } from 'react-router-dom'

import { QUERY_NODE } from '../../graphql/queries'
import { Node, NodeVariables } from '../../graphql/__types__/Node'

const useNode = (serialNumber: string) => {
    const { push: goTo } = useHistory()
    const { data, loading } = useQuery<Node, NodeVariables>(
        QUERY_NODE,
        {
            variables: { serialNumber },
            fetchPolicy: 'cache-and-network',
            onError: (err) => {
                if (err.networkError) goTo('/error', { error: "System API not reachable" })
                else if (err.graphQLErrors.length > 0) goTo('/error', { error: "Could not fetch the sensor node"})
                else goTo('error')
            }
        }
    )

    return { data: (data ? data.node : null), loading }
}

export default useNode
