import React from 'react'
import { Switch, Route } from 'react-router-dom'

import HomePage from '../pages/home/HomePage'
import NodePage from '../pages/node/NodePage'
import ErrorPage from '../pages/error/ErrorPage'

const Routes: React.FC = () => (
    <Switch>
        <Route exact path='/' component={HomePage} />
        <Route exact path='/node/:serialNumber' component={NodePage} />
        <Route component={ErrorPage} />
    </Switch>
)

export default Routes
