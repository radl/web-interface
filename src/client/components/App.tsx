import React from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import { HttpLink, ApolloClient, InMemoryCache } from 'apollo-boost'
import { ApolloProvider } from '@apollo/react-hooks'

import './App.scss'

import { Provider as SettingsProvider } from './contexts/SettingsContext'
import Routes from './routes/Routes'

const client = new ApolloClient({
    link: new HttpLink({ uri: 'http://localhost:4000/api/graphql' }),
    cache: new InMemoryCache()
})

const App: React.FC = () => (
    <ApolloProvider client={client}>
        <Router>
            <SettingsProvider>
                <Routes />
            </SettingsProvider>
        </Router>
    </ApolloProvider>
)

export default App
