import React from 'react'
import { useHistory } from 'react-router-dom'

import styles from './ErrorPage.scss'

import PageLayout from '../../shared/layouts/page-layout/PageLayout'

type LocationStateError = {
    error?: string
}

type LocationState = LocationStateError | null | undefined

const ErrorPage: React.FC = () => {
    const defaultMessage = "Something went wrong"
    const { location: { state: locationState } } = useHistory<LocationState>()

    return (
        <PageLayout title="Error Page" className={[styles.errorPage]} homeButton>
            <div className={styles.message}>{ locationState?.error || defaultMessage }</div>
        </PageLayout>
    )
}

export default ErrorPage
