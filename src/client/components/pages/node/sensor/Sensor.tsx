import React, { useEffect, useContext } from 'react'
import { ResponsiveContainer, LineChart, XAxis, YAxis, Line, Tooltip } from 'recharts'
import moment from 'moment'

import { getClassNames } from '../../../../utils/classnames'
import styles from './Sensor.scss'

import { SettingsID, Context } from '../../../contexts/SettingsContext'
import LoadingLayout from '../../../shared/layouts/loading-layout/LoadingLayout'
import Checkbox from '../../../shared/checkbox/Checkbox'
import { Result } from '../../../hooks/useSensor'

const classNames = getClassNames(styles)

type DataFetcher = () => Result

type Props = {
    dataFetcher: DataFetcher
    id: SettingsID
    name: string
    unit?: string
    step?: boolean
}

const Sensor: React.FC<Props> = ({ dataFetcher, id, name, unit, step }) => {
    const [fetch, { data, loading }] = dataFetcher()
    const { state, setState } = useContext(Context)

    const updateSettings = (value: boolean) => {
        let newState = { ...state }
        newState[id] = value
        setState(newState)
    }

    useEffect(() => {
        if (state[id]) fetch()
    }, [state[id]])

    return (
        <div className={classNames(styles.sensor, { closed: !state[id] })}>
            <div className={styles.title}>
                <Checkbox value={state[id]} onChange={updateSettings} />
                <div className={styles.name}>{name} Logs {unit && `(- ${unit})`}</div>
            </div>
            { state[id] && <LoadingLayout active={loading}>
                <ResponsiveContainer width='99%'>
                    <LineChart data={data}>
                        <XAxis
                            dataKey='time'
                            domain={['auto', 'auto']}
                            tickFormatter={(unixTime) => moment(unixTime).format('HH:mm:ss')}
                            tickMargin={10}
                            type='number'
                            name='Time'
                        />
                        <YAxis
                            dataKey='value'
                            padding={{ top: 30, bottom: 30 }}
                            domain={['dataMin', 'dataMax']}
                            tickMargin={10}
                            name={name}
                        />
                        <Tooltip
                            labelFormatter={(unixTime) => 'Time: ' + moment(unixTime).format('HH:mm:ss')}
                            isAnimationActive={false}
                        />
                        <Line
                            dot={false}
                            dataKey='value'
                            strokeWidth={2}
                            stroke='#666'
                            type={step ? 'stepAfter' : 'monotone'}
                            animationDuration={1000}
                            name={name}
                        />
                    </LineChart>
                </ResponsiveContainer>
                {data.length == 0 && <div className={styles.noData}>No data available</div>}
            </LoadingLayout>}
        </div>
    )
}

export default Sensor
