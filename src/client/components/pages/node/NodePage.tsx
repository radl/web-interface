import React, { useContext } from 'react'
import { useParams, useHistory } from 'react-router-dom'

import styles from './NodePage.scss'

import useNode from '../../hooks/useNode'
import useTemperature from '../../hooks/useTemperature'
import useHumidity from '../../hooks/useHumidity'
import useCO2Level from '../../hooks/useCO2Level'
import useLightIntensity from '../../hooks/useLightIntensity'
import useSoundLevel from '../../hooks/useSoundLevel'
import useMotionDetector from '../../hooks/useMotionDetector'
import { withContext, Context } from '../../contexts/DateRangeContext'
import DateRangeInput from '../../shared/date-range-input//DateRangeInput'
import PageLayout from '../../shared/layouts/page-layout/PageLayout'
import LoadingLayout from '../../shared/layouts/loading-layout/LoadingLayout'
import NodeState from '../../shared/node-state/NodeState'
import Separator from '../../shared/separator/Separator'
import Sensor from './sensor/Sensor'

type Params = {
    serialNumber: string
}

const NodePage: React.FC = () => {
    const { push: goTo } = useHistory()
    const { serialNumber } = useParams<Params>()
    const { data, loading } = useNode(serialNumber)
    const { state: dateRange, setState: setDateRange } = useContext(Context)

    if (!loading && !data) goTo('/error', "Could not find the sensor node")

    return (
        <PageLayout title={serialNumber} className={[styles.nodePage]} homeButton>
            <LoadingLayout active={loading}>
                <div className={styles.state}>
                    {data && <NodeState node={data}/>}
                </div>
                <DateRangeInput value={dateRange} onSubmit={setDateRange} />
                <div className={styles.charts}>
                    <Separator /><Sensor id='temperature' dataFetcher={() => useTemperature(serialNumber)} name='Temperature' unit='°C' />
                    <Separator /><Sensor id='humidity' dataFetcher={() => useHumidity(serialNumber)} name='Humidity' unit='%' />
                    <Separator /><Sensor id='co2Level' dataFetcher={() => useCO2Level(serialNumber)} name='CO2 Level' unit='PPM' />
                    <Separator /><Sensor id='lightIntensity' dataFetcher={() => useLightIntensity(serialNumber)} name='Light Intensity' unit='Lux' />
                    <Separator /><Sensor id='soundLevel' dataFetcher={() => useSoundLevel(serialNumber)} name='Sound Level' unit='dB' />
                    <Separator /><Sensor id='motionDetector' dataFetcher={() => useMotionDetector(serialNumber)} name='Motion Detector' step />
                </div>
            </LoadingLayout>
        </PageLayout>
    )
}

export default withContext(NodePage)
