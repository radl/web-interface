import React from 'react'

import styles from './HomePage.scss'

import useNodes from '../../hooks/useNodes'
import PageLayout from '../../shared/layouts/page-layout/PageLayout'
import LoadingLayout from '../../shared/layouts/loading-layout/LoadingLayout'
import SensorNode from './sensor-node/SensorNode'

const HomePage: React.FC = () => {
    const { data, loading } = useNodes()

    return (
        <PageLayout title="Home Page" className={[styles.homePage]}>
            <LoadingLayout active={loading}>
                <div className={styles.nodes}>
                    {
                        data.map( node => <SensorNode node={node} key={node.serialNumber} />)
                    }
                </div>
            </LoadingLayout>
        </PageLayout>
    )
}

export default HomePage
