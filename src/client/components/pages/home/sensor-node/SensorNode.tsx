import React from 'react'
import { useHistory } from 'react-router-dom'

import styles from './SensorNode.scss'

import logo from '../../../../assets/sensor.svg'
import { Nodes_nodes as Node } from '../../../../graphql/__types__/Nodes'
import NodeState from '../../../shared/node-state/NodeState'

type Props = {
    node: Node
}

const SensorNode: React.FC<Props> = ({ node }) => {
    const { push: goTo } = useHistory()

    return (
        <div className={styles.sensorNode} onClick={() => { goTo(`/node/${node.serialNumber}`) }}>
            <div className={styles.deviceName}>
                {node.deviceName}
            </div>
            <img src={logo} />
            <div className={styles.serialNumber}>
                {node.serialNumber}
            </div>
            <NodeState node={node}/>
        </div>
    )
}

export default SensorNode
