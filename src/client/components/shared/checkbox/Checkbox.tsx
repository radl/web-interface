import React from 'react'

import { getClassNames } from '../../../utils/classnames'
import styles from './Checkbox.scss'

const classNames = getClassNames(styles)

type Props = {
    value: boolean
    onChange: (value: boolean) => void
}

const Checkbox: React.FC<Props> = ({ value, onChange }) => (
    <div
        className={styles.checkbox}
        onClick={() => onChange(!value)}
    >
        <input
            type="checkbox"
            className={styles.input}
            value={value ? 'checked' : undefined}
            onChange={() => onChange(!value)}
        />
        <div className={styles.box}>
            <span className={classNames({ checked: value })} />
        </div>
    </div>
)

export default Checkbox
