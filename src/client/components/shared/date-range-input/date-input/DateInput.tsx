import React from 'react'

import styles from './DateInput.scss'

import { DateTime, DateTimeKey, dateToDateTime, isMissingValue, validate, padding } from '../../../../utils/time'

type Props = {
    onChange: (key: DateTimeKey, value: string) => void
    placeholder?: DateTime
    value: DateTime
}

const DateInput: React.FC<Props> = ({ onChange, value, placeholder = dateToDateTime(new Date()) }) => {
    let error = ''

    const createOnBlur = (key: DateTimeKey) => () => onChange(key, padding(value[key], key === 'year' ? 4 : 2))
    const createOnChange = (key: DateTimeKey) => ({ target: { value } }: React.ChangeEvent<HTMLInputElement>) => {
        if (value === '' || !isNaN(parseInt(value)) && value.length <= (key === 'year' ? 4 : 2)) onChange(key, value)
    }

    if (!isMissingValue(value)) validate(value, (err: string) => { error = err })

    return (
        <div className={styles.dateInput}>
            <input
                value={value.day}
                onChange={createOnChange('day')}
                onBlur={createOnBlur('day')}
                placeholder={placeholder.day}
                className={styles.small}
            />
            /
            <input
                value={value.month}
                onChange={createOnChange('month')}
                onBlur={createOnBlur('month')}
                placeholder={placeholder.month}
                className={styles.small}
            />
            /
            <input
                value={value.year}
                onChange={createOnChange('year')}
                onBlur={createOnBlur('year')}
                placeholder={placeholder.year}
                className={styles.big}
            />
            -|-
            <input
                value={value.hour}
                onChange={createOnChange('hour')}
                onBlur={createOnBlur('hour')}
                placeholder={placeholder.hour}
                className={styles.small}
            />
            :
            <input
                value={value.minute}
                onChange={createOnChange('minute')}
                onBlur={createOnBlur('minute')}
                placeholder={placeholder.minute}
                className={styles.small}
            />
            :
            <input
                value={value.second}
                onChange={createOnChange('second')}
                onBlur={createOnBlur('second')}
                placeholder={placeholder.second}
                className={styles.small}
            />
            { error.length > 0 && <div className={styles.error}>{error}</div>}
        </div>
    )
}

export default DateInput
