import React, { useState } from 'react'

import styles from './DateRangeInput.scss'

import { DateRange } from '../../contexts/DateRangeContext'
import { DateTime, DateTimeKey, dateToDateTime, dateTimeToDate, isMissingValue, validate } from '../../../utils/time'
import DateInput from './date-input/DateInput'
import Button from '../button/Button'

type Props = {
    value: DateRange
    onSubmit: (dateRange: DateRange) => void
}

const DateRangeInput: React.FC<Props> = ({ value, onSubmit }) => {
    const [from, setFrom] = useState<DateTime>(dateToDateTime(value.from))
    const [to, setTo] = useState<DateTime>(dateToDateTime(value.to))

    const createOnChange = (dateTime: DateTime, setter: React.Dispatch<React.SetStateAction<DateTime>>) => (key: DateTimeKey, value: string) => {
        let newDateTime = { ...dateTime }

        newDateTime[key] = value
        setter(newDateTime)

        return newDateTime
    }
    
    const update = () =>  onSubmit({ from: dateTimeToDate(from), to: dateTimeToDate(to) })

    let disabled = false
    const disable = () => { disabled = true }

    if (isMissingValue(from) || isMissingValue(to)) disable()
    else {
        validate(from, disable)
        validate(to, disable, from)
    }

    return (
        <div className={styles.dateRangeInput}>
            <div className={styles.range}>
                <div className={styles.date}>
                    <div className={styles.label}>From:</div>
                    <DateInput value={from} onChange={createOnChange(from, setFrom)} />
                </div>
                <div className={styles.date}>
                    <div className={styles.label}>To:</div>
                    <DateInput value={to} onChange={createOnChange(to, setTo)} />
                </div>
            </div>
            <div className={styles.button}>
                <Button disabled={disabled} onClick={update}>Update</Button>
            </div>
        </div>
    )
}

export default DateRangeInput
