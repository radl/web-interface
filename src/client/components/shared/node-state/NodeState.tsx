import React from 'react'

import { getClassNames } from '../../../utils/classnames'
import styles from './NodeState.scss'

import { formatTimeAgo } from '../../../utils/time'
import { Nodes_nodes as Node } from '../../../graphql/__types__/Nodes'

const classNames = getClassNames(styles)

type Props = {
    node: Node
}

const NodeState: React.FC<Props> = ({ node }) => (
    <table className={styles.nodeState}>
        <tbody>
            <tr className={styles.connected}>
                <th>Status:</th>
                <td className={classNames({ green: node.connected, red: !node.connected })}>
                    {node.connected ? 'On' : 'Off'}
                </td>
            </tr>
            <tr className={styles.lastUpdated}>
                <th>Last updated:</th>
                <td>{formatTimeAgo(node.lastUpdated)}</td>
            </tr>
            <tr className={styles.batteryLevel}>
                <th>Battery Level:</th>
                <td>{node.batteryLevel >= 0 ? node.batteryLevel : '-'} %</td>
            </tr>
        </tbody>
    </table>
)

export default NodeState
