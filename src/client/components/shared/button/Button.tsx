import React from 'react'

import { ClassValues, getClassNames } from '../../../utils/classnames'
import styles from './Button.scss'

const classNames = getClassNames(styles)

type Props = {
    children: string,
    onClick: () => void
    disabled?: boolean
}

const Button: React.FC<Props> = ({ disabled, onClick, children }) => (
    <button
        className={classNames(styles.button, { disabled })}
        onClick={() => { if (!disabled) onClick() }}
    >
        { children }
    </button>
)

export default Button
