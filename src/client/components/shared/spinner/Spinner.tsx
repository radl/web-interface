import React from 'react'

import styles from './Spinner.scss'

const Spinner: React.FC = () => (
    <div className={styles.spinner}>
        {
            Array.from(Array(10).keys()).map( i => (
                <div
                    key={`spinner-item-${i}`}
                    className={styles.spinnerItem}
                />
            ))
        }
    </div>
)

export default Spinner
