import React from 'react'
import { useHistory } from 'react-router-dom'

import { ClassValues, getClassNames } from '../../../../utils/classnames'
import styles from './PageLayout.scss'

import Button from '../../button/Button'

const classNames = getClassNames(styles)

type Props = {
    title: string
    homeButton?: boolean
    className?: ClassValues
}

const PageLayout: React.FC<Props> = ({ title, homeButton, className = [], children }) => {
    const { push: goTo } = useHistory()

    return (
        <div className={styles.pageLayout}>
            <h1>{title}</h1>
            { homeButton
                ? <div className={styles.button}><Button onClick={() => { goTo('/') }}>Home</Button></div>
                : null
            }
            <div className={classNames(...className)}>{children}</div>
        </div>
    )
}

export default PageLayout
