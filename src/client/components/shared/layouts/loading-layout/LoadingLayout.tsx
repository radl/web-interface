import React, { Fragment } from 'react'

import styles from './LoadingLayout.scss'

import Spinner from '../../spinner/Spinner'

type Props = {
    active?: boolean
}

const LoadingLayout: React.FC<Props> = ({ active, children }) => (
    <Fragment>
        { active
            ? <div className={styles.loadingLayout}>
                <h3>Loading...</h3>
                <Spinner />
            </div>
            : <Fragment>
                { children }
            </Fragment>
        }
    </Fragment>
)

export default LoadingLayout
