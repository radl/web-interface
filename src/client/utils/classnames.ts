import CN from 'classnames/bind'

import { ClassValue } from 'classnames/types'

export type ClassValues = ClassValue[]

type Styles = {
    [className: string]: string
}

export const getClassNames = (styles: Styles) => CN.bind(styles)
