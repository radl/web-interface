export const formatTimeAgo = (str: string) => {
    const seconds = Math.floor((new Date().valueOf() - new Date(str).valueOf()) / 1000)
    let interval = Math.floor(seconds / 31556952)

    if (interval >= 1) return interval + " years ago"

    interval = Math.floor(seconds / 2628000)
    if (interval >= 1) return interval + " months ago"

    interval = Math.floor(seconds / 86400)
    if (interval >= 1) return interval + " days ago"

    interval = Math.floor(seconds / 3600)
    if (interval >= 1) return interval + " hours ago"

    interval = Math.floor(seconds / 60)
    if (interval >= 1) return interval + " minutes ago"

    interval = Math.floor(seconds)
    if (interval >= 1) return interval + " seconds ago"
    else return "now"
}

export const padding = (value: string | number, min: number) => {
    const newValue = typeof value === 'string' ? value : value.toString()
    if (newValue === '' || newValue.length >= min) return newValue
    else return new Array(min - newValue.length + 1).join('0') + newValue
}

const isWithin = (value: string, min: number, max: number) => min <= parseInt(value) && parseInt(value) <= max
const daysInMonth = (month: string, year: string) => new Date(parseInt(year), parseInt(month), 0).getDate()

export type DateTimeKey = 'year' | 'month' | 'day' | 'hour' | 'minute' | 'second'

export type DateTime = {
    year: string
    month: string
    day: string
    hour: string
    minute: string
    second: string
}

export const isMissingValue = (date: DateTime) => date.day === '' || date.month === '' || date.year === '' || date.hour === '' || date.minute === '' || date.second === ''

export const dateToDateTime = (value: Date): DateTime => ({
    year: padding(value.getFullYear(), 4),
    month: padding(value.getMonth() + 1, 2),
    day: padding(value.getDate(), 2),
    hour: padding(value.getHours(), 2),
    minute: padding(value.getMinutes(), 2),
    second: padding(value.getSeconds(), 2)
})

export const dateTimeToDate = (date: DateTime) => new Date(parseInt(date.year), parseInt(date.month)-1, parseInt(date.day), parseInt(date.hour), parseInt(date.minute), parseInt(date.second))

export const validate = (date: DateTime, onFail: (error: string) => void, min?: DateTime) => {
    if (!isWithin(date.month, 1, 12)) onFail("Invalid date: month out of range")
    else if (!isWithin(date.year, 1970, new Date().getFullYear())) onFail("Invalid date: year out of range")
    else if (!isWithin(date.day, 1, daysInMonth(date.month, date.year))) onFail("Invalid date: day out of range")
    else if (!isWithin(date.hour, 0, 23)) onFail("Invalid time: hour out of range")
    else if (!isWithin(date.minute, 0, 59)) onFail("Invalid time: minute out of range")
    else if (!isWithin(date.second, 0, 59)) onFail("Invalid time: second out of range")
    else if (dateTimeToDate(date).valueOf() > Date.now()) onFail("Cannot input future date")
    else if (min && dateTimeToDate(date).valueOf() <= dateTimeToDate(min).valueOf()) onFail("Invalid: lower than minimum")
}
