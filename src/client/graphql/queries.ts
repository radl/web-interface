import gql from 'graphql-tag'

export const QUERY_NODES = gql`
    query Nodes {
        nodes {
            serialNumber
            deviceName
            connected
            lastUpdated
            batteryLevel
        }
    }
`

export const QUERY_NODE = gql`
    query Node($serialNumber: String!) {
        node(serialNumber: $serialNumber) {
            serialNumber
            deviceName
            connected
            lastUpdated
            batteryLevel
        }
    }
`

export const QUERY_TEMPERATURE = gql`
    query Temperature($serialNumber: String!, $from: String, $to: String) {
        node(serialNumber: $serialNumber) {
            sensors(from: $from, to: $to) {
                temperature {
                    time
                    value
                }
            }
        }
    }
`

export const QUERY_HUMIDITY = gql`
    query Humidity($serialNumber: String!, $from: String, $to: String) {
        node(serialNumber: $serialNumber) {
            sensors(from: $from, to: $to) {
                humidity {
                    time
                    value
                }
            }
        }
    }
`

export const QUERY_CO2_LEVEL = gql`
    query CO2Level($serialNumber: String!, $from: String, $to: String) {
        node(serialNumber: $serialNumber) {
            sensors(from: $from, to: $to) {
                co2Level {
                    time
                    value
                }
            }
        }
    }
`

export const QUERY_LIGHT_INTENSITY = gql`
    query LightIntensity($serialNumber: String!, $from: String, $to: String) {
        node(serialNumber: $serialNumber) {
            sensors(from: $from, to: $to) {
                lightIntensity {
                    time
                    value
                }
            }
        }
    }
`

export const QUERY_SOUND_LEVEL = gql`
    query SoundLevel($serialNumber: String!, $from: String, $to: String) {
        node(serialNumber: $serialNumber) {
            sensors(from: $from, to: $to) {
                soundLevel {
                    time
                    value
                }
            }
        }
    }
`

export const QUERY_MOTION_DETECTOR = gql`
    query MotionDetector($serialNumber: String!, $from: String, $to: String) {
        node(serialNumber: $serialNumber) {
            sensors(from: $from, to: $to) {
                motionDetector {
                    time
                    value
                }
            }
        }
    }
`
