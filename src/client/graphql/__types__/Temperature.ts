/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Temperature
// ====================================================

export interface Temperature_node_sensors_temperature {
  __typename: "SensorInt";
  time: OffsetDateTime;
  value: number;
}

export interface Temperature_node_sensors {
  __typename: "Sensors";
  temperature: Temperature_node_sensors_temperature[];
}

export interface Temperature_node {
  __typename: "Node";
  sensors: Temperature_node_sensors;
}

export interface Temperature {
  node: Temperature_node | null;
}

export interface TemperatureVariables {
  serialNumber: string;
  from?: string | null;
  to?: string | null;
}
