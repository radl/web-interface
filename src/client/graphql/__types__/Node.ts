/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Node
// ====================================================

export interface Node_node {
  __typename: "Node";
  serialNumber: string;
  deviceName: string;
  connected: boolean;
  lastUpdated: OffsetDateTime;
  batteryLevel: number;
}

export interface Node {
  node: Node_node | null;
}

export interface NodeVariables {
  serialNumber: string;
}
