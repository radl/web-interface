/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SoundLevel
// ====================================================

export interface SoundLevel_node_sensors_soundLevel {
  __typename: "SensorInt";
  time: OffsetDateTime;
  value: number;
}

export interface SoundLevel_node_sensors {
  __typename: "Sensors";
  soundLevel: SoundLevel_node_sensors_soundLevel[];
}

export interface SoundLevel_node {
  __typename: "Node";
  sensors: SoundLevel_node_sensors;
}

export interface SoundLevel {
  node: SoundLevel_node | null;
}

export interface SoundLevelVariables {
  serialNumber: string;
  from?: string | null;
  to?: string | null;
}
