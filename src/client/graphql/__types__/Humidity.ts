/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Humidity
// ====================================================

export interface Humidity_node_sensors_humidity {
  __typename: "SensorInt";
  time: OffsetDateTime;
  value: number;
}

export interface Humidity_node_sensors {
  __typename: "Sensors";
  humidity: Humidity_node_sensors_humidity[];
}

export interface Humidity_node {
  __typename: "Node";
  sensors: Humidity_node_sensors;
}

export interface Humidity {
  node: Humidity_node | null;
}

export interface HumidityVariables {
  serialNumber: string;
  from?: string | null;
  to?: string | null;
}
