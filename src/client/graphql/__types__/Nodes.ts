/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Nodes
// ====================================================

export interface Nodes_nodes {
  __typename: "Node";
  serialNumber: string;
  deviceName: string;
  connected: boolean;
  lastUpdated: OffsetDateTime;
  batteryLevel: number;
}

export interface Nodes {
  nodes: Nodes_nodes[];
}
