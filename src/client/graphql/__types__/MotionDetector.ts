/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: MotionDetector
// ====================================================

export interface MotionDetector_node_sensors_motionDetector {
  __typename: "SensorBoolean";
  time: OffsetDateTime;
  value: boolean;
}

export interface MotionDetector_node_sensors {
  __typename: "Sensors";
  motionDetector: MotionDetector_node_sensors_motionDetector[];
}

export interface MotionDetector_node {
  __typename: "Node";
  sensors: MotionDetector_node_sensors;
}

export interface MotionDetector {
  node: MotionDetector_node | null;
}

export interface MotionDetectorVariables {
  serialNumber: string;
  from?: string | null;
  to?: string | null;
}
