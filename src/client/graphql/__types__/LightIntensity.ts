/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: LightIntensity
// ====================================================

export interface LightIntensity_node_sensors_lightIntensity {
  __typename: "SensorInt";
  time: OffsetDateTime;
  value: number;
}

export interface LightIntensity_node_sensors {
  __typename: "Sensors";
  lightIntensity: LightIntensity_node_sensors_lightIntensity[];
}

export interface LightIntensity_node {
  __typename: "Node";
  sensors: LightIntensity_node_sensors;
}

export interface LightIntensity {
  node: LightIntensity_node | null;
}

export interface LightIntensityVariables {
  serialNumber: string;
  from?: string | null;
  to?: string | null;
}
