/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: CO2Level
// ====================================================

export interface CO2Level_node_sensors_co2Level {
  __typename: "SensorInt";
  time: OffsetDateTime;
  value: number;
}

export interface CO2Level_node_sensors {
  __typename: "Sensors";
  co2Level: CO2Level_node_sensors_co2Level[];
}

export interface CO2Level_node {
  __typename: "Node";
  sensors: CO2Level_node_sensors;
}

export interface CO2Level {
  node: CO2Level_node | null;
}

export interface CO2LevelVariables {
  serialNumber: string;
  from?: string | null;
  to?: string | null;
}
