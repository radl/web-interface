import express, { Express } from 'express'
import helmet from 'helmet'
import cors from 'cors'

export default {
    start: (before?: (express: Express) => void) => {
        const PORT = 8080
        const app = express()

        app.use(cors())
        app.use(helmet())

        if (before) before(app)

        app.listen(PORT, () => {
            console.log(`Listening on http://localhost:${PORT}`)
        })
    }
}
