import webpackDevMiddleware from 'webpack-dev-middleware'
import webpackHotMiddleware from 'webpack-hot-middleware'
import connect from 'connect-history-api-fallback'
import webpack from 'webpack'

import { devConfig } from '../webpack/client.config'
import server from './server'

const compiler = webpack(devConfig)

if (process.env.NODE_ENV === 'development') {
    server.start((app) => {
        app.use(connect())
        app.use(webpackDevMiddleware(compiler))
        app.use(webpackHotMiddleware(compiler))
    })
} else console.error("Error: NODE_ENV is not set to development")
