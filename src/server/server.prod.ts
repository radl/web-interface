import express from 'express'
import path from 'path'
import fs from 'fs'

import server from './server'

const PATH = path.resolve(__dirname, './client/')
const HTML_PATH = path.join(PATH, './index.html')

if (fs.existsSync(HTML_PATH)) {
    server.start((app) => {
        app.use(express.static(PATH))
        app.get('/*', (req, res) => {
            res.sendFile(HTML_PATH)
        })
    })
} else console.error("Error: HTML page not found")
