import webpack from 'webpack'
import merge from 'webpack-merge'
import nodeExternals from 'webpack-node-externals'
import path from 'path'

const common: webpack.Configuration = {
    target: 'node',
    externals: [
        nodeExternals()
    ],
    node: {
        __dirname: false,
        __filename: false
    },
    output: {
        path: path.resolve(__dirname, '../../out'),
        publicPath: '/'
    },
    resolve: {
        extensions: ['.ts', '.js']
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            }
        ]
    }
}

const dev: webpack.Configuration = {
    entry: './src/server/server.dev.ts',
    mode: 'development',
    devtool: 'cheap-eval-source-map',
    output: {
        filename: 'server.dev.js'
    }
}

const prod: webpack.Configuration = {
    entry: './src/server/server.prod.ts',
    mode: 'production',
    optimization: {
        minimize: true
    },
    output: {
        filename: 'server.js'
    }
}

export default merge(common, process.env.NODE_ENV === 'development' ? dev : prod)
