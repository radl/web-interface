import HtmlWebpackPlugin from 'html-webpack-plugin'
import webpack from 'webpack'
import merge from 'webpack-merge'
import path from 'path'

const common: webpack.Configuration = {
    entry: [
        './src/client/index.tsx'
    ],
    target: 'web',
    output: {
        path: path.resolve(__dirname, '../../out/client'),
        publicPath: '/'
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js']
    },
    module: {
        rules: [
            {
                test: /\.(ts|tsx)$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader',
                        options: { modules: true }
                    },
                    {
                        loader: 'sass-loader'
                    }
                ]
            },
            {
                test: /\.(png|svg)$/,
                exclude: /node_modules/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    outputPath: 'assets'
                }
            }
        ]
    }
}

const dev: webpack.Configuration = {
    entry: [
        'webpack-hot-middleware/client'
    ],
    mode: 'development',
    output: {
        filename: 'client.dev.js'
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, '../src/client/index.html'),
            filename: 'index.html'
        })
    ]
}

const prod: webpack.Configuration = {
    mode: 'production',
    optimization: {
        minimize: true
    },
    output: {
        filename: 'client.js'
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, '../client/index.html'),
            filename: 'index.html'
        })
    ]
}

export default merge(common, process.env.NODE_ENV === 'development' ? dev : prod)

export const devConfig = merge(common, dev)
