declare module '*.scss' {
    const content: { [className: string]: string }
    export default content
}

declare module '*.svg' {
    const content: string
    export default content
}

// GraphQL Scalars
declare type OffsetDateTime = string;
